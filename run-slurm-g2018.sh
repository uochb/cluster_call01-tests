#!/bin/bash
#SBATCH -J test
#SBATCH --time=0-YYY
#SBATCH --nodes=XXX
#SBATCH --exclusive
#SBATCH -p ZZZ

#Gromacs 2018 installed using spack
source /home/martinez/software/spack/share/spack/setup-env.sh
module load fftw-3.3.7-gcc-7.3.0-5jf66oj
module load openmpi-3.0.0-gcc-7.3.0-ideodjl
module load gromacs-2018-gcc-7.3.0-7ciroyb

NAME="test_md"
mpirun gmx_mpi mdrun -deffnm ${NAME} -dlb yes -v -maxh YYY
