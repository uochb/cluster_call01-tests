#!/bin/bash

test_name="test03"
partition="gpu" #Select nodes corresponding to the big memory nodes
num_nodes_job=(1 2 5) #number of nodes used in the test job
num_gpu_nodes=5 #Total number of big memory nodes in the cluster
time_job=1 #hour
for n in ${num_nodes_job[@]}; do
   num_jobs=$((${num_gpu_nodes}/$n))
   for ((i=1; i<=${num_jobs}; i++ )); do
      JOBDIR="$test_name/`printf "%02d" $n`/`printf "%02d" $i`"
      echo $JOBDIR
      mkdir -p $JOBDIR
      cp test_md.tpr $JOBDIR
      cp run-slurm-g2018-gpu.sh  $JOBDIR
      cd $JOBDIR
      sed -i "s/XXX/$n/" run-slurm-g2018-gpu.sh #Set number nodes job
      sed -i "s/YYY/$time_job/" run-slurm-g2018-gpu.sh #Set time job
      sed -i "s/ZZZ/$partition/" run-slurm-g2018-gpu.sh #Set real name gpu nodes partition
      sbatch run-slurm-g2018-gpu.sh #Modify accordingly to the target cluster
      cd ../../../
   done 
done

exit 0

# Examples on how to gather the performance results from the 2 Nodes test jobs:
grep Performance test03/02/*/test_md.log | awk '{print $2}'
