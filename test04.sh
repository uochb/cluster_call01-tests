#!/bin/bash

test_name="test04"
num_cpu_nodes=150 #Substitute by the real total number of cpu nodes in the cluster
num_mem_nodes=8  # Total number of big memory nodes in the cluster
num_gpu_nodes=5  # Total number of gpu nodes in the cluster
time_job=23 # Hours

num_cpu_jobs=$((${num_cpu_nodes}/2))
for ((i=1; i<=${num_cpu_jobs}; i++ )); do
   JOBDIR="$test_name/cpu/`printf "%02d" $i`"
   echo $JOBDIR
   mkdir -p $JOBDIR
   cp test_md.tpr $JOBDIR
   cp run-slurm-g2018.sh  $JOBDIR
   cd $JOBDIR
   sed -i "s/XXX/2/" run-slurm-g2018.sh
   sed -i "s/YYY/$time_job/" run-slurm-g2018.sh
   sed -i "s/ZZZ/cpu/" run-slurm-g2018.sh #Set time job
   sbatch run-slurm-g2018.sh #Modify accordingly to the target cluster
   cd ../../../
done

num_mem_jobs=$((${num_mem_nodes}/2))
for ((i=1; i<=${num_mem_jobs}; i++ )); do
   JOBDIR="$test_name/mem/`printf "%02d" $i`"
   echo $JOBDIR
   mkdir -p $JOBDIR
   cp test_md.tpr $JOBDIR
   cp run-slurm-g2018.sh  $JOBDIR
   cd $JOBDIR
   sed -i "s/XXX/2/" run-slurm-g2018.sh
   sed -i "s/YYY/$time_job/" run-slurm-g2018.sh
   sed -i "s/ZZZ/mem/" run-slurm-g2018.sh #Set time job
   sbatch run-slurm-g2018.sh #Modify accordingly to the target cluster
   cd ../../../
done

num_gpu_jobs=$((${num_gpu_nodes}/1))
for ((i=1; i<=${num_gpu_jobs}; i++ )); do
   JOBDIR="$test_name/gpu/`printf "%02d" $i`"
   echo $JOBDIR
   mkdir -p $JOBDIR
   cp test_md.tpr $JOBDIR
   cp run-slurm-g2018-gpu.sh  $JOBDIR
   cd $JOBDIR
   sed -i "s/XXX/1/" run-slurm-g2018-gpu.sh
   sed -i "s/YYY/$time_job/" run-slurm-g2018-gpu.sh
   sed -i "s/ZZZ/gpu/" run-slurm-g2018-gpu.sh #Set time job
   sbatch run-slurm-g2018-gpu.sh #Modify accordingly to the target cluster
   cd ../../../
done
exit 0

# Examples on how to gather the performance results from the test jobs:
grep Performance test04/cpu/*/test_md.log | awk '{print $2}'
grep Performance test04/gpu/*/test_md.log | awk '{print $2}'
grep Performance test04/mem/*/test_md.log | awk '{print $2}'
