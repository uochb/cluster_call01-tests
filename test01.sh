#!/bin/bash

test_name="test01"
partition="cpu" #Select nodes corresponding to the computing nodes
num_nodes_job=(2 4 6 8 10 12 16 18 20) #number of nodes used in the test job
num_cpu_nodes=150 #Substitute by the real total number of cpu nodes in the cluster
time_job=1 #hour
for n in ${num_nodes_job[@]}; do
   num_jobs=$((${num_cpu_nodes}/$n))
   for ((i=1; i<=${num_jobs}; i++ )); do
      JOBDIR="$test_name/`printf "%02d" $n`/`printf "%02d" $i`"
      echo $JOBDIR
      mkdir -p $JOBDIR
      cp test_md.tpr $JOBDIR
      cp run-slurm-g2018.sh  $JOBDIR
      cd $JOBDIR
      sed -i "s/XXX/$n/" run-slurm-g2018.sh #Set number nodes job
      sed -i "s/YYY/$time_job/" run-slurm-g2018.sh #Set time job
      sed -i "s/ZZZ/$partition/" run-slurm-g2018.sh #Set real name computing nodes partition
      sbatch run-slurm-g2018.sh #Modify accordingly to the target cluster
      cd ../../../
   done 
done

exit 0

# Examples on how to gather the performance results from the 2 Nodes test jobs:
grep Performance test01/02/*/test_md.log | awk '{print $2}'
