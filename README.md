# Tests required to be passed for the acceptance of the cluster
## Disclaimer
- The following test might require some tuning by the provider to make them work on the purchased cluster. Typos or incorrect syntax in the templates and scripts provided do not exempt the provider from passing the tests. 

## General definitions
- A test can contain several tasks, and each task may contain several jobs.
- We define a task as a collection of jobs which have similar parameters defined in the test.
- A job is an own parallel execution (i.e., using mpi/gpu) to be executed in a unique set of nodes.

## General considerations:
- All test must successfully pass without exception.
- All the tasks (i.e., group of jobs) within a test must be submitted to the job scheduler consecutively.
- When executing a test, no time gaps between the submission of tasks are acceptable beyond those produced by the job scheduler when functioning normally.
- All the jobs in each task must run simultaneously and use a unique set of nodes.
- Small differences in the starting execution times of the jobs within a task are acceptable when produced by the job scheduler when functioning normally.
- Each of the jobs within a task must exceed the minimal performance prerequisite established for the task.
- It is the responsibility of the provider to adjust the minimal instructions provided in the **sample script** for each test to run in the cluster in a way that fulfills the target performance.
- Job lanching scrips (run-slurm-g2018.sh and run-gpu-slurm-g2018.sh) called in the sample scripts have been validated for [Slurm](https://slurm.schedmd.com/) and use Gromacs installed using [Spack](https://spack.readthedocs.io/). It is the responsibility of the provider to adjust these scripts to the target cluster. 
- The results of the tests must be provided for inspection to ensure their validity.
- The tests have to be performed using Gromacs 2018.3 or newer - The program has a GPLv3 license and can be downloaded for free from the following [page](http://www.Gromacs.org/).

## General terms and abbreviations:
- Number of Nodes and their types
   - $`N_{cpu}`$ = Total number of computational nodes
   - $`N_{gpu}`$ = Total number of GPU nodes
   - $`N_{mem}`$ = Total number of big memory nodes
   - $`N = N_{cpu}+N_{gpu}+N_{mem}`$= Total number of nodes in the cluster
   - $`N^{test}_{task,job}`$ = Number of nodes to be used in a job for a given task belonging to a test
- Number of jobs required to run simultaneously within a task
   - $`J_{cpu}^{max} = floor interger part of (N_{cpu}/N^{test}_{task,job})`$ (Maximum number of simulatneous jobs in a task using computing nodes)
   - $`J_{gpu}^{max} = floor interger part of (N_{gpu}/N^{test}_{task,job})`$ (Maximum number of simulatneous jobs in a task using gpu nodes)
   - $`J_{mem}^{max} = floor interger part of (N_{mem}/N^{test}_{task,job})`$ (Maximum number of simulatneous jobs in a task using big memory nodes

## 1. Tests scaling in the computing nodes
- **Test name:**: test01
- **Short description of the system**:  Molecular dynamics simulation of a micelle membrane interaction in Gromacs.
- **Number atoms:** 172742
- **Template script file for the test:** test01.sh
- **Short description of the test**: Full ocupation of the computational nodes using 2 or more nodes per job. There might be few unallocaded nodes if $`N_{cpu}`$ is not a multiple of $`N^{test}_{task,job}`$.
- **Number of tasks:** 10
- **Execution time per task:** 1 hour
- **Number of computational nodes per job in each task:**  $`N^{test01}_{task,job}=`$ 2, 4, 6, 8, 10, 12, 14, 16, 18 and 20 Nodes.
- **Total duration test:** $`\sim 1 \text{hour per task} \times 10 = 10 \text{hours}`$
- **Number of simultaneous jobs in each task:** $`J_{cpu}^{max}`$
- **Special test conditions:**
   - All tasks in the test need to be run sequentially without interruption to ensure that some jobs eventually use nodes placed in different sections of the cluster.
- **Minimal performance for each job:**

| Nodes  |  2  |  4  |  6  |  8  |  10 |  12 |  14 |  16 |  18 |  20 |
| ------ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| ns/day |  14 |  29 |  41 |  50 |  61 |  75 |  80 |  87 |  97 | 110 |


## 2. Tests scaling in big memory nodes
- **Test name:**: test02
- **Short description of the system**:  Molecular dynamics simulation of a micelle membrane interaction in Gromacs.
- **Number atoms:** 172742
- **Template script file for the test:** test02.sh
- **Short description of the test**: Full ocupation of the computational nodes using 2 or more nodes per job. There might be few unallocaded nodes if $`N_{mem}`$ is not a multiple of $`N^{test}_{task,job}`$.
- **Number of tasks:** 4
- **Execution time per task:** 1 hour
- **Number of computational nodes per job in each task:**  $`N^{test02}_{task,job}=`$ 2, 4, 6, and 8 nodes.
- **Total duration test:** $`\sim 1 \text{hour per task} \times 4 = 4 \text{hours}`$
- **Number of simultaneous jobs in each task:** $`J_{cpu}^{max}`$
- **Special test conditions:**
   - All tasks in the test need to be run sequentially without interruption to ensure that some jobs eventually use nodes placed in different sections of the cluster.
- **Minimal performance for each job:**

| Nodes  |  2  |  4  |  6  |  8  |
| ------ | --- | --- | --- | --- |
| ns/day |  14 |  29 |  41 |  50 |


## 3. Tests scaling in the gpu nodes
- **Test name:**: test03
- **Short description of the system**:  Molecular dynamics simulation of a micelle membrane interaction in Gromacs.
- **Number atoms:** 172742
- **Template script file for the test:** test02.sh
- **Short description of the test**: Full ocupation of the computational nodes using 2 or more nodes per job. There might be few unallocaded nodes if $`N_{gpu}`$ is not a multiple of $`N^{test}_{task,job}`$.
- **Number of tasks:** 3
- **Execution time per task:** 1 hour
- **Number of computational nodes per job in each task:**  $`N^{test03}_{task,job}=`$ 1, 2, 5 nodes.
- **Total duration test:** $`\sim 1 \text{hour per task} \times 3 = 3 \text{hours}`$
- **Number of simultaneous jobs in each task:** $`J_{gpu}^{max}`$
- **Special test conditions:**
   - All tasks in the test need to be run sequentially without interruption to ensure that some jobs eventually use nodes placed in different sections of the cluster.
- **Minimal performance for each job:**

| Nodes  |  1  |  2  |  5  |
| ------ | --- | --- | --- |
| ns/day |  14 |  29 |  60 |


## 4. Test endurance cluster
- **Test name:**: test04
- **Short description of the system**:  Molecular dynamics simulation of a micelle membrane interaction in Gromacs.
- **Number atoms:** 172742
- **Template script file for the test:** test04.sh
- **Short description of the test:** Full occupation of all nodes (CPU, GPU, big memory) using 2 nodes per job (computing and big memory nodes) and 1 node in the GPU. It is a stress test where all the cluster should work under full load for at least 23h.
- **Number of tasks:** 3
- **Number of computational nodes per job in each task:**  $`N^{test04}_{cpu-task,job}=2`$ nodes;  $`N^{test04}_{mem-task,job}=2`$ nodes;  $`N^{test04}_{gpu-task,job}=1`$ node.
- **Number of simultaneous jobs in each task:** $`J_{cpu}^{max} + J_{gpu}^{max} + J_{mem}^{max}`$
- **Execution time per task:** 23 hours
- **Total duration test:** 23 hours
- **Minimal performance for each job:** Similar (within 10%) to the one obtained in test01 (2 nodes), test02 (2 nodes), and test03 (1 node).
- **Special test conditions:**
   - All jobs in the 3 tasks must run for the fixed period of 23h without interruptions.

